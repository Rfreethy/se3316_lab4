// server.js



//************** DATA BASE INIT *********************
var mongoose   = require('mongoose');
mongoose.connect('mongodb://localhost/lab4');
var db = mongoose.connection;

var Person = require('./app/models/person');
var Post = require('./app/models/post');
var ObjectID = require('mongodb').ObjectID;


//************** EXPRESS INIT *********************
var express    = require('express');        // call express
var app = express();

app.use(express.static('public'));//this will set public files

var router = express.Router();




//************** BODY PARSER INIT *********************
//Let us read JSON posts
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


var port = process.env.PORT || 8080; // set our port



//************** THESE ARE MY ROUTES *********************

router.route('/send-post').post(function(req, res){
    
    var newPost = new Post();
    newPost.author = req.body.author;
    newPost.content = req.body.content;
    newPost.likes = 0;

    newPost.save(function(err){
        if(err){
            res.send(err);
            return;
        }
    })
});

router.route('/like-post').post(function(req, res){
    
    var id = new ObjectID(req.body._id);
    
    db.collection('posts').updateOne(
      { _id : id },
      {
        $set: { "likes":  req.body.likes}
      }    
    );
    
    
});


router.route('/sign-in').post(function(req, res){
    
    //count all the peple with a matchin ID
    Person.count({"ID": req.body.ID}, function (err, count){ 
      //if there is already one, done add to DB
      
      //if any match...
      if(count>0){
        //DO NOTHING!!
        
      }else{//else, add the user to the databse
        var newPerson = new Person();
        newPerson.ID = req.body.ID;
        newPerson.Name = req.body.Name;
      
        newPerson.save(function(err){
          if(err){
            res.send(err);
            return;
          }
        })
        }
    }); 
  
})


router.get('/posts', function(req, res, next) {
  Post.find(function(err, posts){
    if(err){ return next(err); }

    res.json(posts);
    
  });
});



//************** END OF MY ROUTES *********************


app.use('/', router);
// START THE SERVER
// =============================================================================
app.listen(port);

