var app = angular.module('myApp', []);

var profile;
var needReload = false;

app.factory('postsFactory', ['$http', 
function($http){
    
    //create object called o, which has a property caled persons
    var o = { posts: [] };
    //populate o's person array with perso
    o.getAll = function() {
        $http.get('/posts').success(function(data){
            angular.copy(data, o.posts);
        });
    };
    
    return o;
}]);


app.controller('dbCont', [ '$scope', 'postsFactory','$http',

function($scope, postsFactory, $http){
    
    setVisibleDivs();
    postsFactory.getAll();
    $scope.posts = postsFactory.posts;
    
    $scope.likePost = function(id, likes){
        var newLikeValue = likes + 1;
        var data =  JSON.stringify({ "_id": id, "likes":newLikeValue});
        $http.post("/like-post", data);
        
        setTimeout(function(){
            postsFactory.getAll();
            $scope.posts = postsFactory.posts;
        },300);
    }
        
    $scope.sendPost = function(){
        
        var data =  JSON.stringify({ "author": $scope.currentUser, "content":$scope.newPost});
        if($scope.newPost != null){
            $http.post("/send-post", data);
        }
        //after post is send, clear text area
        $scope.newPost = null;
        
        setTimeout(function(){
            postsFactory.getAll();
            $scope.posts = postsFactory.posts;
        },300);
    }
    
    setTimeout(function(){
        if(profile != null){
            var data =  JSON.stringify({ "ID": profile.getId(), "Name":profile.getName()});
            $http.post("/sign-in", data);
            $scope.currentUser = profile.getName();
            setVisibleDivs();
        }else{
            needReload = true;
        }
    },1000);
    
}]);





app.filter('reverse', function() {
  return function(items) {
    return items.slice().reverse();
  };
});


function onSignIn(googleUser){
    profile = googleUser.getBasicProfile();
    if(needReload == true){
        location.reload();
    }
}


function setVisibleDivs(){
    if(profile != null){
        jQuery('#google-sign-in').css("display","none");
        jQuery('#make-post').css("display","block");
    }
    
    if(profile == null){
        jQuery('#make-post').css("display","none");
        jQuery('#google-sign-in').css("display","block");
    }
}


