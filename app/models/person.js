var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var PersonSchema   = new Schema({
    ID: Number,
    Name: String
});

module.exports = mongoose.model('Person', PersonSchema);