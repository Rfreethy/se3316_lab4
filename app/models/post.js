var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var PostSchema   = new Schema({
    author: String,
    content: String,
    likes: Number
});

module.exports = mongoose.model('Post', PostSchema);